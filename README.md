NAME
====

Local Github Issues - fetch and render Github.com issues as HTML

SYNOPSIS
========

```
cd $HOME/important/your-fancy-github-repo
fetch-github-issues -v
# Re-run with a 1 hour break if the GH rate limiter limits your rate.
# HTML files will be stored unter ./github-issues/ .
```

INSTALL
=======

```
apt install gh libcmark-dev; zef install JSON::Fast Cmark 'Git::Config:auth<github:gfldex>';
cp fetch-github-issues.raku $HOME/bin/
```

Setting up the Github.com CLI with `gh auth` may be required. Instrunctions can be found [here](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/about-authentication-to-github) and [here](https://cli.github.com/manual/gh_auth_login).

Named Arguments
===============

| Argument         | Default Value            | Purpose |
| :--------------- | :----------------------- | :------ |
| `--repo-dir`     | `.`                      | Where to look for `.git/config`. |
| `--basename`     | `issues`                 | Basename for `$basename`.html and `$basename`-index.html, actually a path-fragment. |
| `--wd`           | `./github-issues`        | Where to store local files. |
| `--cache-dir`    | `./github-issues/cache`  | Directory to store cache files in. |
| `--no-index`     | False                    | Don't create `issues-index.html`. |
| `--no-cache`     | False                    | Do not cache. Note, this may hit github.com rate limits. |
| `-v` `--verbose` | False                    | This very option makes the script very verbose indeed. |

AUTHOR
======

Wenzel P. P. Peppmeyer <gfldex> <wenzel.peppmeyer@gmx.de>
Source: https://codeberg.org/gfldex/raku-local-github-issues

COPYRIGHT AND LICENSE
=====================

Copyright Ⓒ 2023 Wenzel P. P. Peppmeyer

This program is free software; you can redistribute it and/or modify it under the GNU General Public License Version 3.
