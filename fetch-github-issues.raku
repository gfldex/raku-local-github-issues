#! /usr/bin/env raku
# 
# Local Github Issues - fetch and render github.com issues as html
# Copyright © 2023 Wenzel P. P. Peppmeyer <gfldex> wenzel.peppmeyer@gmx.de
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

use v6.*;

# INSTALL:
#
# apt install gh libcmark-dev; zef install Cmark 'Git::Config:auth<github:gfldex>';

use JSON::Fast;
use Git::Config:auth<github:gfldex>;

# TODO check for `gh --version`

my (&markdown-to-html, &inline-code-to-html) = {
    use Cmark;
    constant options = CMARK_OPT_UNSAFE +| CMARK_OPT_VALIDATE_UTF8 +| CMARK_OPT_HARDBREAKS;

    sub markdown-to-html(Str() $markdown) {

        Cmark.parse($markdown, options).to-html
    },
    sub inline-code-to-html(Str:D $markdown) {
        $markdown.subst(/ '`' (<-[`]>+) '`'/, { q:c⟨<code class='inline'>{$0}</code>⟩ }, :g)
    }
}();

sub prefix:<DZ>(Int:D $i) {
    $i.chars < 2 ?? '0' ~ $i !! $i
}

constant NOP = -> | { };

my &note = NOP;

multi sub MAIN(
    IO() :repo-dir($*repo-dir) = '.', # Where to look for .git/config .
    Str :basename($*basename) = 'issues', #= Basename for $basename.html and $basename-index.html, actually a path-fragment.
    IO() :wd($*wd) = './github-issues', #= Where to store local files.
    IO() :cache-dir($*cache-dir) is copy, #= Directory to store cache files in.
    Bool :$no-index = False, #= Don't create issues-index.html .
    Bool :no-cache($*no-cache) = False, #= Do not cache. Note, this may hit github.com rate limits.
    Bool :v(:verbose($*verbose)) = False, #= This option makes the script very verbose indeed.
) {
    $*cache-dir //= $*wd.add('cache');

    mkdir($*wd) unless $*wd.e;
    mkdir($*cache-dir) unless $*cache-dir.e ^^ $*no-cache;

    &note = &CORE::note if $*verbose;
    my &add-index-entry = $no-index ?? NOP !! &OUTER::add-index-entry unless $no-index;

    my \issues = fetch-issues;

    note "writing to: ⌊$*wd/$*basename.html⌋";

    spurt "$*wd/$*basename.html", Q:c ⟨<!DOCTYPE html>{$?NL}<html><head><style>{issues-css}</style><link rel='stylesheet' href="{$*basename}.css"/><script>{javascript}</script></head><body class='githubissues'>⟩ ~
    Q:c ⟨<div id='gui-filter' class='gui-filter hidden' title='Hit <Esc> to jump into the filter field.'><label>Filter:</label><input id='filter-input' placeholder='-o -c @author #title or words in the isse/comment body' /></div>⟩ ~
        issues.hyper(:batch(8), :degree(8)).map({
            .<title>.=&inline-code-to-html;
            add-index-entry(.<number>, .<title>, :closed(.<closed>));
            my $state = .<closed> ?? 'closed' !! 'open';
            q:c:to /■/
            <div class="issue {$state}" id="{.<number>}">
                <div class='header'>
                    <div class='title'><a href="{.<url>}"><span class='number'>{.<number>}</span></a> - <span class='title'>{.<title>}</span></div>
                    <div class='author'><span class='author'>@{.<author><login>}</span> opend this issue at <time datetime="{.<createdAt>}">{DateTime.new(.<createdAt>, :formatter({"{.year}-{DZ.month}-{DZ.day} {DZ.hour}:{DZ.minute}"}))}</time></div>
                </div>
                <div class='body'>
                    { markdown-to-html .<body> }
                </div>
                <div class='comments'>
                    { render-comments(.<comments>) }
                </div>
            </div>
            ■
        }).join ~
        '</body></html>';
}

multi sub MAIN(
    Bool :$dump-css #= Write the CSS-file to STDOUT. Use the filename issues.css and edit it to customise the CSS locally.
) {
    put issues-css;
}

sub fetch-issues {
    my int $issue-number = 1;

    sub fetch-issue($number) {
        indir $*repo-dir, {
            qx:c<gh issue view -c {$number} --json=id,number,title,author,url,body,comments,createdAt,closed>
        }
    }

    gather for from-json(qx<gh issue list --state=all --limit=1000000 --json=number,updatedAt>).map({ .<number>, DateTime.new(.<updatedAt>) }).sort(-*.[0].Int) -> [$issue-number, $last-changed] {

        my $cache-file = $*cache-dir.add($issue-number);
        my $json;

        if $*no-cache || $cache-file.e && $cache-file.modified ≥ $last-changed {
            note „cache hit on $issue-number“;

            take from-json slurp $cache-file;

            CATCH { default { try $cache-file.unlink; redo } }
        } else {
            note „fetching issue $issue-number“;

            $json = fetch-issue $issue-number;
            try spurt($cache-file, $json);
            take from-json $json;
        }
    }
}

sub render-comments(@comments) {
    @comments.map({
        q:c:to /■/
        <div class='comment' id="{.<id>}">
            <div class='header'>
                <span class='author'>@{.<author><login>}</span> commented at <time datetime="{.<createdAt>}">{DateTime.new(.<createdAt>, :formatter({"{.year}-{DZ.month}-{DZ.day} {DZ.hour}:{DZ.minute}"}))}</time>
            </div>
            <div class='body'>
                { markdown-to-html .<body> }
            </div>
        </div>
        ■
    }).join
}

sub add-index-entry($number, $title is copy, :$closed is copy) {
    $_ = once { 
        $_ = open("$*wd/{$*basename}-index.html", :w);
        .put: Q:c ⟨<html><head><style>{issues-css}</style><link rel='stylesheet' href='issues.css'/></head><body class='githubissues index'><div class='index'>⟩;
        $_
    }
    END { with $*wd { .put: Q ⟨</div></body></html>⟩; .close } }

    $closed = $closed ?? '<span class="state closed">[closed]</span>' !! ('<span class="state open">[open]</closed>' but False);

    .put: Q:c ⟨<div class="entry { $closed ?? 'closed' !! 'open' }"><a href="issues.html#{$number}"><span class='number'>{$number}</span></a> {$closed} <span class='title'>{$title}</span></div>⟩;
}

sub issues-css {
q:to /■/;
@charset "utf-8";

html {
    font-family: serif;
    font-size: 14pt;
    font-variant-ligatures: common-ligatures no-discretionary-ligatures no-historical-ligatures contextual;
    font-variant-numeric: oldstyle-nums;
}

body {
    margin-left: 3em;
    margin-right: 3em;
}

.issue > .body {
    margin-left: 2em;
}

.issue.open div.header div.author span.author::before {
    content: "☐ ";
    color: #1a7f37;
}

.issue.closed div.header div.author span.author::before {
    content: "☑ ";
    color: #8250df;
}

.issue > .comment {
    margin-left: 2em;
}

.issue .header {
    font-size: 140%;
    background-color: #f0f0f0;
}

.issue > .comments {
    margin-left: 2em;
}

.issue .comment {
    padding-top: 1em;
}

.issue blockquote {
    border-left: .25em solid #a0a0a0;
    padding-left: 1em;
}

.issue h1 {
    font-size: 130%;
}

.issue h2 {
    font-size: 115%;
}

div.gui-filter {
    width: 100%;
    display: table;
    margin-bottom: 1em;
}

div.gui-filter label {
    width: 0;
    padding-right: 1em;
    display: table-cell;
}

div.gui-filter input {
    width: 100%;
    display: table-cell;
}

.hidden {
    display: none !important;
}

body.index > .issue .state,.closed {
    color: #505050;
}

■
}

sub javascript {
q:to /■/;
addEventListener('load', (event) => { 
    (window.inputFilter = document.getElementById('gui-filter')).classList.remove('hidden');
    inputFilter.addEventListener('change', event => {
        var tests = []; // Contains functions that return true if  the element is to be shown.
        var text = event.target.value;
        if (text == '') {
            tests.push(el => true);
        } else {
            if (text.startsWith('-c')) {
                tests.push( el => el.classList.contains('open') );
                text = text.substr(2).trim();
            } else if (text.startsWith('-o')) {
                tests.push( el => el.classList.contains('closed') );
                text = text.substr(2).trim();
            }

            if (text.startsWith('@')) {
                let needle = text;
                tests.push( el => el.querySelector('span.author').innerText.startsWith(needle) );
                text = text.split(' ').slice(1).join(' ').trim();
            }

            if (text.startsWith('#')) {
                let needle = new RegExp(text.substr(1), 'i');
                tests.push( el => el.querySelector('div.header span.title').innerText.search(needle) != -1 );
            } else if (text != "") {
                let needles = text.split(' ').map(e => { let re = new RegExp(e, 'i'); return text => re.test(text) });
                tests.push( el => all(el.innerText, needles));
            }
        }

        document.querySelectorAll('div.issue').forEach(el => {
            if (all(el, tests)) {
                show(el);
            } else {
                hide(el);
            }
        });
    });

    document.addEventListener('keydown', ev => {
        if (ev.key == "Escape") {
            inputFilter.querySelector('input#filter-input').focus({focusVisible: true});
        }
    });

    function hide(el) { el.classList.add('hidden'); return el }
    function show(el) { el.classList.remove('hidden'); return el }

    function all(el, list) {
        for (const fun of list) {
            if ( !fun(el) ) {
                return false;
            }
        }
        return true;
    }
});
■
}
